# K9S Configuration

This project holds my [k9s](https://k9scli.io/) configuration files.

## Install

To utilize these files simply clone this repository into `$XDG_CONFIG_HOME/k9s` and see the changes take effect.

## K9S Configuration References

- [Configuration](https://k9scli.io/topics/config/)
- [Columns](https://k9scli.io/topics/columns/)
- [Skins](https://k9scli.io/topics/skins/)
  - [Pre-Made Skins](https://github.com/derailed/k9s/tree/master/skins)
